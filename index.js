
const FIRST_NAME = "Valsan ";
const LAST_NAME = "Adela Maria";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() {
    var empty={};
    empty.pageAccessCounter=function(paginaURL='home'){
        paginaURL=new String(paginaURL).toLowerCase();
         if(empty.hasOwnProperty(paginaURL)){
            empty[paginaURL]=empty[paginaURL]+1;
        }
        else{
            Object.defineProperty(empty,paginaURL,{
                value:1,
                writable:true
            });
        }
    }
    empty.getCache=function(){
        return this;
    }
    return empty;   
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}


